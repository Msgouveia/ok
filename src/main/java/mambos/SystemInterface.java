package mambos;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public interface SystemInterface {

    boolean isPoll(String id);

    void addUser(String id, String name);

    void loadUser(String id, String name, int xp);

    void increaseXp(String id);

    void addFrase(String frase) throws IOException;

    void addFeed(String feed) throws IOException;

    void addMain(String main) throws IOException;

    void addPoll(String id, String name);

    void suggest(String sugestao, String nickName) throws IOException;

    public String getPoll(String id);

    String getFrase();

    String getMain();

    String getVibe();

    String getMc();

    String getLane();

    String getFeed();

    String getFeliz();

    User getUser(String id);

    User getUserName(String name);

    Iterator<String> getSuggestList() throws FileNotFoundException;

    Iterator<User> userIterator();

    Iterator<User> topIterator();

    Pair<Iterator<String>, Iterator<String>> getTeams(List<String> teamList);

}
