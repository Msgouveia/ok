package mambos;

public interface Pair<F, S> {

    F getFirst();

    S getSecond();

}
