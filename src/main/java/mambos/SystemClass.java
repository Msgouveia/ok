package mambos;

import Exceptions.AlreadyExistsException;
import Exceptions.InvalidNumberException;
import Exceptions.UserDoesNotExistException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class SystemClass implements SystemInterface{

    private final List<String> frases;
    private final List<Pair<String, String>> suggest;
    private final List<String> mainList;
    private final List<String> vibeList;
    private final List<String> mcList;
    private final List<String> laneList;
    private final List<String> feedList;
    private final Map<String, User> userList;
    private final Map<String, String> pollList;

    public SystemClass() throws IOException {

        userList = new HashMap<>();
        pollList = new HashMap<>();

        //Frases
        frases = new ArrayList<>();
        loadFrase();//Heroku

        //Champions
        mainList = new ArrayList<>();
        loadMains();//Heroku

        //Vibe
        vibeList = new ArrayList<>();
        vibeList.add("https://www.youtube.com/watch?v=yI6GwHgXqRc");
        vibeList.add("https://www.youtube.com/watch?v=udI21J9xY8A");
        vibeList.add("https://www.youtube.com/watch?v=epDORRdUbsM");
        vibeList.add("https://www.youtube.com/watch?v=GYaPeZKrgdg");
        vibeList.add("https://www.youtube.com/watch?v=6DPwzeDJtwc");

        //Timzudo
        mcList = new ArrayList<>();
        mcList.add("https://www.youtube.com/watch?v=i2vhBI0rtF4");
        mcList.add("https://www.youtube.com/watch?v=lIWvrfPbpck");
        mcList.add("https://www.youtube.com/watch?v=oTTYLRiSEoU");
        mcList.add("https://www.youtube.com/watch?v=RtVnr4gaaYk");
        mcList.add("https://www.youtube.com/watch?v=JhaxdRcvhZk");
        mcList.add("https://www.youtube.com/watch?v=LGDdN5Ks4AY");
        mcList.add("https://www.youtube.com/watch?v=hgwC3DNvcBU");
        mcList.add("https://www.youtube.com/watch?v=4RXGGeEx9Ow");
        mcList.add("https://www.youtube.com/watch?v=tYwNQsjeQlU");
        mcList.add("https://www.youtube.com/watch?v=rReWmFBo47k&t");

        //Lanes
        laneList = new ArrayList<>();
        laneList.add("TOP");
        laneList.add("JNG");
        laneList.add("MID");
        laneList.add("BOT");
        laneList.add("SUP");

        //Feed
        feedList = new ArrayList<>();
        loadFeed();//Heroku

        //
        suggest = new ArrayList<>();
        loadSuggest();//Heroku

    }

    @Override
    public boolean isPoll(String id) {
        return pollList.containsKey(id);
    }

    @Override
    public void addUser(String id, String name) {
        if(!userList.containsKey(id)){
            userList.put(id, new UserClass(id, name));
        }
    }

    @Override
    public void loadUser(String id, String name, int xp) {
        if(!userList.containsKey(id)){
            userList.put(id, new UserClass(id, name, xp));
        }
    }

    @Override
    public void increaseXp(String id) throws UserDoesNotExistException{
        if(userList.get(id) == null){
            throw new UserDoesNotExistException();
        }
        userList.get(id).increaseXp();
    }

    @Override
    public void addFrase(String frase) throws IOException, AlreadyExistsException {
        if(frases.contains(frase)){
            throw new AlreadyExistsException();
        }
        frases.add(frase);
        save();//Heroku
    }

    @Override
    public void addFeed(String feed) throws IOException, AlreadyExistsException {
        if(feedList.contains(feed)){
            throw new AlreadyExistsException();
        }
        feedList.add(feed);
        save();//Heroku
    }

    @Override
    public void addMain(String main) throws IOException, AlreadyExistsException {
        if(mainList.contains(main)){
            throw new AlreadyExistsException();
        }
        mainList.add(main);
        save();//Heroku

    }

    @Override
    public void addPoll(String id, String name) {
        pollList.put(id, name);
    }

    @Override
    public void suggest(String sugestao, String nickName) throws IOException, UserDoesNotExistException {
        if(sugestao == null || nickName == null || sugestao == "" ||nickName == ""){
            throw new UserDoesNotExistException();
        }
        suggest.add(new PairClass<>(nickName, sugestao));
        save();//Heroku
    }

    @Override
    public String getPoll(String id) {
        return pollList.get(id);
    }

    @Override
    public String getFrase() {
        int random = new Random().nextInt(frases.size());
        return frases.get(random);
    }

    @Override
    public String getMain() {
        int random = new Random().nextInt(mainList.size());
        return mainList.get(random);
    }

    @Override
    public String getVibe() {
        int random = new Random().nextInt(vibeList.size());
        return vibeList.get(random);
    }

    @Override
    public String getMc() {
        int random = new Random().nextInt(mcList.size());
        return mcList.get(random);
    }

    @Override
    public String getLane() {
        int random = new Random().nextInt(laneList.size());
        return laneList.get(random);
    }

    @Override
    public String getFeed() {
        int random = new Random().nextInt(feedList.size());
        return feedList.get(random);
    }

    @Override
    public String getFeliz() {
        int random = new Random().nextInt(101);
        String feliz = null;

        if(random<=20){
            feliz = random + "% - J ta bue triste";
        }
        if(random>20 && random<=40){
            feliz = random + "% - J ta triste";
        }
        if(random>40 && random<=60){
            feliz = random + "% - J ta com mood normal";
        }
        if(random>60 && random<=80){
            feliz = random + "% - J ta com vibe feliz";
        }
        if(random>80){
            feliz = random + "% - J ta super contente";
        }
        return feliz;
    }

    @Override
    public User getUser(String id) throws UserDoesNotExistException{

        if(userList.get(id) == null){
            throw new UserDoesNotExistException();
        }
        return userList.get(id);
    }

    @Override
    public User getUserName(String name) {
        for(User user: userList.values()){
            if(user.getName().equalsIgnoreCase(name)){
                return user;
            }
        }
        return null;
    }

    @Override
    public Iterator<String> getSuggestList() throws FileNotFoundException {

        List<String> lista = new LinkedList<>();

        File folder = new File("files");
        folder.mkdir();

        File file = new File(folder, ("suggest.txt"));

        Scanner in = new Scanner(file);


        while(in.hasNext()){
            String nickName = in.nextLine().trim();
            String suggest = in.nextLine().trim();

            lista.add(nickName);
            lista.add(suggest);
        }

        in.close();

        return lista.iterator();
    }

    @Override
    public Iterator<User> userIterator() {
        List<User> list = new LinkedList<>(userList.values());
        Collections.sort(list);

        return list.iterator();
    }

    @Override
    public Iterator<User> topIterator() throws InvalidNumberException{

        if(userList.size() < 3){
            throw new InvalidNumberException();
        }

        List<User> sortedList = new LinkedList<>(userList.values());
        Collections.sort(sortedList);

        List<User> topList = new ArrayList<>();

        for(int i = 0; i < 3; i++){
            topList.add(i ,sortedList.get(i));
        }

        return topList.iterator();
    }

    @Override
    public Pair<Iterator<String>, Iterator<String>> getTeams(List<String> teamList) throws InvalidNumberException {

        if((teamList.size() > 21) || teamList.size() < 2){
            throw new InvalidNumberException();
        }

        LinkedList<String> teamA, teamB;

        Collections.shuffle(teamList);

        teamA = new LinkedList<>();
        teamB = new LinkedList<>();

        for(int i = 0; i<(teamList.size()/2); i++){
            teamA.add(teamList.get(i));
        }

        for(int i = (teamList.size()/2); i<teamList.size(); i++){
            teamB.add(teamList.get(i));
        }

        Pair pair = new PairClass(teamB.iterator(), teamA.iterator());

        return pair;

    }

    private void saveFrase() throws IOException {

        //File folder = new File("files");


        File file = new File("frases.txt");

        FileWriter out = new FileWriter(file);

        Iterator<String> it = frases.iterator();

        while(it.hasNext()){
            String frase = it.next();
            out.write(frase + "\n");
        }
        out.close();

    }

    private void loadFrase() throws IOException {

        /*File folder = new File("files");
        folder.mkdir();

        File file = new File(folder, ("frases.txt"));*/

        File file = new File("frases.txt");

        Scanner in = new Scanner(file);


        while(in.hasNext()){
            String frase = in.nextLine().trim();
            frases.add(frase);
        }

        in.close();
    }

    private void saveFeed() throws IOException {
        //File folder = new File("files");


        File file = new File("feed.txt");

        FileWriter out = new FileWriter(file);

        Iterator<String> it = feedList.iterator();

        while(it.hasNext()){
            String feed = it.next();
            out.write(feed + "\n");
        }
        out.close();
    }

    private void loadFeed() throws IOException {

        /*File folder = new File("files");
        folder.mkdir();

        File file = new File(folder, ("feed.txt"));*/

        File file = new File("feed.txt");

        Scanner in = new Scanner(file);


        while(in.hasNext()){
            String feed = in.nextLine().trim();
            feedList.add(feed);
        }

        in.close();
    }

    private void saveMains() throws IOException {
        //File folder = new File("files");


        File file = new File("mains.txt");

        FileWriter out = new FileWriter(file);

        Iterator<String> it = mainList.iterator();

        while(it.hasNext()){
            String main = it.next();
            out.write(main + "\n");
        }
        out.close();
    }

    private void loadMains() throws IOException {
/*
        File folder = new File("files");
        folder.mkdir();

        File file = new File(folder, ("mains.txt"));*/

        File file = new File("mains.txt");

        Scanner in = new Scanner(file);


        while(in.hasNext()){
            String main = in.nextLine().trim();
            mainList.add(main);
        }

        in.close();
    }

    private void saveSuggest() throws IOException {
        //File folder = new File("files");


        File file = new File("suggest.txt");

        FileWriter out = new FileWriter(file);

        Iterator<Pair<String, String>> it = suggest.iterator();

        while(it.hasNext()){
            Pair<String, String> pair = it.next();
            out.write(pair.getFirst() + "\n" + pair.getSecond() + "\n");
        }
        out.close();
    }

    private void loadSuggest() throws FileNotFoundException {
        /*File folder = new File("files");
        folder.mkdir();

        File file = new File(folder, ("suggest.txt"));*/

        File file = new File("suggest.txt");

        Scanner in = new Scanner(file);


        while(in.hasNext()){
            String name = in.nextLine().trim();
            String suggest = in.nextLine().trim();
            this.suggest.add(new PairClass<>(name, suggest));
        }

        in.close();
    }

    private void save() throws IOException {
        saveFrase();
        saveFeed();
        saveMains();
        saveSuggest();
    }

}
