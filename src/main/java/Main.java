/*
@author Timzudo
version 3.10
TODO playlist mambo grosso / library youtube playlist youtube data api / bugs meter a funcionar em todos os servers / meter roles / cooldown mover / dm / comando chorae
ideias minigame /
 */


import Exceptions.InvalidFileException;
import Exceptions.InvalidNumberException;
import Exceptions.UserDoesNotExistException;
import audio.PlayerManager;
import mambos.User;
import mambos.*;
import net.dv8tion.jda.api.*;
import net.dv8tion.jda.api.audit.ActionType;
import net.dv8tion.jda.api.audit.AuditLogEntry;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent;
import net.dv8tion.jda.api.events.guild.voice.GenericGuildVoiceEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.managers.AudioManager;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class Main extends ListenerAdapter {

    private static final String VERSION = "MinguitoBot v3.10";

    private static final String PRINTINFO = "$ping: ping.\n" +
            "$mini: para de feedar.\n" +
            "$classico: classico mini...\n" +
            "$meda: comando especifico para mandar o meda para o caralho.\n" +
            "$hugana: classico hugana.\n" +
            "$j: novo main do j.\n" +
            "$feed: mostra a cara do maior feeder da tuga.\n" +
            "$frase: frase aleatoria do mini.\n" +
            "$equipas: faz 2 equipas a toa.\n" +
            "$1v1: faz 1v1 entre dois users.\n" +
            "$5v5: faz equipas para 5v5.\n" +
            "$reroll: da reroll nas equipas.\n" +
            "$lane: manda uma lane a toa.\n" +
            "$xp: lista de users por XP.\n" +
            "$top: top 3 users por XP.\n" +
            "$vibe: mete a vibe da sala no ponto.\n" +
            "$mc: mete som bem rebentado na sala.\n" +
            //"$mambo: playlist mambo grosso.\n" +
            //"$shuffle: shuffle na playlist.(Beta)\n" +
            "$skip: da skip na musica.\n" +
            "$stop: estraga a vibe.\n" +
            "$water liga a proteção de water™.\n" +
            "$say: mete o bot a falar.\n" +
            "$sound: toggle do som do bot.(Admin)\n" +
            "$userinfo: mostra a informação de um user.\n" +
            "$poll: fazer polls \n" +
            "$sugerir: sugere novos comandos para o bot.\n" +
            "$slista: lista de sugestões.(Admin)\n" +
            "$poll: fazer polls \n" +
            "$ip: ip do server de minecraft.";

    private static final String NOVIDADES = "$sugerir\n" + "$reroll\n" + "$1v1\n" + "$poll\n" + "$ip";

    private static final String PING =          "$ping";
    private static final String MINI =          "$mini";
    private static final String CLASSICO =      "$classico";
    private static final String MEDA =          "$meda";
    private static final String HUGANA =        "$hugana";
    private static final String J =             "$j";
    private static final String FELIZ =         "$feliz";
    private static final String FRASE =         "$frase";
    private static final String EQUIPAS =       "$equipas";
    private static final String V =             "$5v5";
    private static final String REROLL =        "$reroll";
    private static final String SIMAO =         "!play";
    private static final String FEED =          "$feed";
    private static final String LANE =          "$lane";
    private static final String ADDFRASE =      "$+frase";
    private static final String ADDFEED =       "$+feed";
    private static final String ADDMAIN =       "$+main";
    private static final String TOP =           "$top";
    private static final String XP =            "$xp";
    private static final String USERINFO =      "$userinfo";
    private static final String SAVE =          "$save";
    private static final String LOAD =          "$load";
    private static final String INFO =          "$info";
    private static final String HELP =          "$help";
    private static final String VIBE =          "$vibe";
    private static final String MC =            "$mc";
    private static final String MAMBO =         "$mambo";
    private static final String SHUFFLE =       "$shuffle";
    private static final String SKIP =          "$skip";
    private static final String STOP =          "$stop";
    private static final String WATER =         "$water";
    private static final String SAY =           "$say";
    private static final String SOUND =         "$sound";
    private static final String SUGGEST =       "$sugerir";
    private static final String SUGGESTLIST =   "$slista";
    private static final String V1 =            "$1v1";
    private static final String POLL =          "$poll";
    private static final String IP =            "$ip";
    private static final String TESTE =         "$teste";

    private static final String TOKEN =     "NzIzNjExNjEyNTU2MTY1MTYx.XvM3sQ.YdK_Vb_RKyyGuUppxfPBbdRu6RQ";
    private static final String ERRO =      "Erro mamboso";
    private static final String IDTIMZUDO = "222675252516225024";
    private static final String IDSIMAO =   "513020940934971392";
    private static final String IDMINI =    "336594936181030913";
    private static final String IDWATER =   "231390816705314816";

    private static boolean water, sound, waiting;
    private static Pair<List<String>, Boolean> reroll;
    private static Pair<String, String> v1;


    private final SystemInterface s = new SystemClass();
    private static JDA jda;


    public Main() throws IOException {
    }

    public static void main(String[] args) throws LoginException, IOException, InterruptedException {
        water = false;
        sound = true;
        waiting = false;
        reroll = null;
        v1 = new PairClass<>("*", "*");

        BasicConfigurator.configure();

        jda = new JDABuilder(AccountType.BOT)
                .setToken(TOKEN)
                .addEventListeners(new Main())
                .setActivity(Activity.listening("MC Timzudo"))
                .build();

        TimeUnit.SECONDS.sleep(1);

    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if(!waiting){
            if(!event.getAuthor().isBot() && (event.getMessage().getContentRaw().length() != 0)){
                Message msg = event.getMessage();
                String msgContent = msg.getContentRaw();
                String[] id = msgContent.split(" ", 2);

                if(msgContent.length() > 0 && msgContent.startsWith("#")){
                    event.getChannel().sendMessage("Novo prefixo: $").queue();
                }

                int random = new Random().nextInt(200);

                if(random == 100){
                    event.getChannel().sendMessage("cala so a boca fds").queue();
                }

                try{
                    processLoad(event, s); //Heroku
                    String userId = event.getAuthor().getId();
                    String userName = event.getAuthor().getName();
                    s.addUser(userId, userName);
                    String prefixo = id[0].substring(0, 1);
                    if(prefixo.equals("$")){
                        switch (id[0]) {
                            case PING:
                                processPing(event, s);
                                break;

                            case MINI:
                                processMini(event, s);
                                break;

                            case CLASSICO:
                                processClassico(event, s);
                                break;

                            case MEDA:
                                processMeda(event, s);
                                break;

                            case HUGANA:
                                processHugana(event, s);
                                break;

                            case J:
                                processJ(event, s);
                                break;

                            case FELIZ:
                                processFeliz(event, s);
                                break;

                            case FRASE:
                                processFrase(event, s);
                                break;

                            case EQUIPAS:
                                processEquipas(event, s);
                                break;

                            case V:
                                process5v5(event, s);
                                break;

                            case REROLL:
                                processReroll(event, s);
                                break;

                            case SIMAO:
                                processSimao(event);
                                break;

                            case FEED:
                                processFeed(event, s);
                                break;

                            case LANE:
                                processLane(event, s);
                                break;

                            case ADDFRASE:
                                processAddFrase(event, s);
                                break;

                            case ADDFEED:
                                processAddFeed(event, s);
                                break;

                            case ADDMAIN:
                                processAddMain(event, s);
                                break;

                            case XP:
                                processLista(event, s);
                                break;

                            case TOP:
                                processTop(event, s);
                                break;

                            case USERINFO:
                                processUserInfo(event, s);
                                break;

                            case LOAD:
                                processLoad(event, s);
                                break;

                            case SAVE:
                                processSave(event, s);
                                break;

                            case INFO:

                            case HELP:
                                processInfo(event);
                                break;

                            case VIBE:
                                processVibe(event, s);
                                break;

                            case MC:
                                processMc(event, s);
                                break;

                            case MAMBO:
                                processMambo(event, s);
                                break;

                            case SHUFFLE:
                                processShuffle(event);
                                break;

                            case SKIP:
                                processSkip(event);
                                break;

                            case STOP:
                                processStop(event);
                                break;

                            case WATER:
                                processWater(event);
                                break;

                            case SAY:
                                processSay(event);
                                break;

                            case SOUND:
                                processSound(event);
                                break;

                            case SUGGEST:
                                processSuggest(event, s);
                                break;

                            case SUGGESTLIST:
                                processSuggestList(event, s);
                                break;

                            case V1:
                                processV1(event, s);
                                break;

                            case POLL:
                                processPoll(event, s);
                                break;

                            case IP:
                                processIp(event, s);
                                break;

                            case TESTE:
                                processTeste(event);
                                break;

                            default:
                                unknownCommand(event);
                                break;
                        }
                    }

                    System.out.println("mensagem");
                    processSave(event, s); //Heroku
                }
                catch (IOException e){
                    MessageChannel channel = event.getChannel();
                    channel.sendMessage(ERRO).queue();
                }
            }
        }
        else {
            String id = event.getTextChannel().getLatestMessageId();
            String name = event.getMessage().getContentRaw();

            s.addPoll(id, name);
            waiting = false;

            event.getChannel().retrieveMessageById(event.getMessageId()).queue(message -> {
                message.addReaction("\uD83C\uDDF8").queue();
                message.addReaction("\uD83C\uDDF3").queue();
            });
        }
    }

    //NICKNAME
    @Override
    public void onGuildMemberUpdateNickname(@Nonnull GuildMemberUpdateNicknameEvent event) {
        super.onGuildMemberUpdateNickname(event);
        System.out.println("nick");
    }

    //CHANGE CHANNEL
    @Override
    public void onGuildVoiceJoin(@NotNull GuildVoiceJoinEvent event) {
        super.onGuildVoiceJoin(event);
        infantario(event);
    }

    @Override
    public void onGuildVoiceMove(@Nonnull GuildVoiceMoveEvent event) {
        super.onGuildVoiceMove(event);
        infantario(event);
    }

    @Override
    public void onGuildVoiceLeave(@Nonnull GuildVoiceLeaveEvent event) {
        super.onGuildVoiceLeave(event);
        if(event.getGuild().getName().equals("Trutas")){
            List<AuditLogEntry> list  = null;
            try {
                list = event.getGuild().retrieveAuditLogs().complete(true);
            } catch (RateLimitedException e) {
                e.printStackTrace();
            }

            AuditLogEntry entry = list.get(0);
            net.dv8tion.jda.api.entities.User user = entry.getUser();

            if(!(user.getId().equals("723611612556165161") || user.isBot() || user.getId().equals("222675252516225024")) && entry.getType().equals(ActionType.MEMBER_VOICE_KICK)){
                event.getGuild().kickVoiceMember(event.getGuild().getMember(list.get(0).getUser())).complete();
                event.getGuild().getDefaultChannel().sendMessage("Para de ser mongo " + user.getName()).queue();
            }
        }
    }

    @Override
    public void onGenericMessageReaction(@Nonnull GenericMessageReactionEvent event) {
        super.onGenericMessageReaction(event);

        if(s.isPoll(event.getMessageId())){

            String name = s.getPoll(event.getMessageId());

            event.getChannel().retrieveMessageById(event.getMessageId()).queue(message -> {
                int pos = -1;
                for(MessageReaction emote: message.getReactions()){
                    String s = emote.getReactionEmote().getName();
                    if(s.equals("\uD83C\uDDF8")){
                        pos += emote.getCount();
                    }
                }
                int neg = -1;
                for(MessageReaction emote: message.getReactions()){
                    String n = emote.getReactionEmote().getName();
                    if(n.equals("\uD83C\uDDF3")){
                        neg += emote.getCount();
                    }
                }


                int posS;
                int negS;

                if((pos+neg) == 0){
                    posS = 0;
                    negS = 0;
                }
                else{
                    posS = (pos*100)/(pos+neg);
                    negS = (neg*100)/(pos+neg);
                }

                StringBuilder msg = new StringBuilder();
                msg.append(name + "\n");

                msg.append("Sim:  ");
                for(int i = 0; i < (posS/5); i++){
                    msg.append("█");
                }
                msg.append(" " + posS + "% \n");

                msg.append("Não: ");
                for(int i = 0; i < (negS/5); i++){
                    msg.append("█");
                }
                msg.append(" " + negS + "% \n");


                message.editMessage(msg).queue();
            });
        }
    }

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        super.onGuildMemberJoin(event);
        event.getUser().openPrivateChannel().queue((channel)->{
            if(event.getGuild().getName().equals("Trutas")){
                channel.sendMessage("Dja bu sabi " + event.getMember().getEffectiveName() + ", espera que alguem te dê roles para poderes falar.").queue();
            }

        });
    }

    private static void unknownCommand(MessageReceivedEvent event){
        event.getChannel().sendMessage("? Usa $help").queue();
    }

    private static void processPing(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        long time = System.currentTimeMillis();
        channel.sendMessage("Pong!") /* => RestAction<Message> */.queue(response /* => Message */ -> response.editMessageFormat("Pong: %d ms", System.currentTimeMillis() - time).queue());
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processMini(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        channel.sendMessage("Juro para de feedar mini! omg").queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processClassico(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        channel.sendMessage("Classico mini, CLASSICO").queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processMeda(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        channel.sendMessage("vai la pro caralho").queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processHugana(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        channel.sendMessage("vou falar com a patroa").queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processJ(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        channel.sendMessage("O novo main do j é: " + s.getMain()).queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processFeliz(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        channel.sendMessage(s.getFeliz()).queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processFrase(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        if(event.getAuthor().getId().equals(IDMINI)){
            channel.sendMessage("cala a boca mini").queue();
        }
        else{
            try{
                String frase = s.getFrase();
                if(sound){
                    MessageBuilder messageBuilder = new MessageBuilder();
                    messageBuilder.append(frase);
                    messageBuilder.setTTS(true);
                    messageBuilder.sendTo(channel).queue();

                }
                else{
                    event.getChannel().sendMessage(frase).queue();
                }
                s.increaseXp(event.getAuthor().getId());
            }
            catch (InvalidNumberException e){
                event.getChannel().sendMessage("Lista vazia").queue();
            }
        }
    }

    private static void processEquipas(MessageReceivedEvent event, SystemInterface s){

        MessageChannel channel = event.getChannel();
        String msgContent = event.getMessage().getContentRaw();
        String[] userList = msgContent.trim().split(" ");
        List<String> nameList = new LinkedList<>();

        nameList.addAll(Arrays.asList(userList).subList(1, userList.length));

        try {

            Pair<EmbedBuilder, EmbedBuilder> pair = teamEmbedBuilder(s.getTeams(nameList));

            EmbedBuilder eb1 = pair.getFirst();
            EmbedBuilder eb2 = pair.getSecond();

            channel.sendMessage(eb1.build()).queue();
            channel.sendMessage(eb2.build()).queue();

            s.increaseXp(event.getAuthor().getId());
            reroll = new PairClass<>(nameList, true);
        }

        catch(InvalidNumberException e){
            channel.sendMessage("fds mete um numero entre 1 e 20, tao ai " + (userList.length - 1)).queue();
        }
    }

    private static void process5v5(MessageReceivedEvent event, SystemInterface s){


        String[] msg = event.getMessage().getContentRaw().split(" ");

        List<Member> memberList = event.getGuild().getVoiceChannelById("731256824786845803").getMembers();
        List<String> nameList = new LinkedList<>();

        boolean deuMerda = false;

        if(memberList.size() == 10){
            for (Member member: memberList){
                nameList.add(member.getEffectiveName());
            }
        }
        else{
            if(msg.length == 11){
                Collections.addAll(nameList, msg);
                nameList.remove(0);
            }
            else{
                deuMerda = true;
                event.getChannel().sendMessage("Mete ai 10 nomes crl").queue();
            }
        }


        if(!deuMerda){
            Pair<EmbedBuilder, EmbedBuilder> pair = vEmbedBuilder(s.getTeams(nameList));

            EmbedBuilder eb1 = pair.getFirst();
            EmbedBuilder eb2 = pair.getSecond();

            event.getChannel().sendMessage(eb1.build()).queue();
            event.getChannel().sendMessage(eb2.build()).queue();

            s.increaseXp(event.getAuthor().getId());
            reroll = new PairClass<>(nameList, false);
        }

    }

    private static void processReroll(MessageReceivedEvent event, SystemInterface s){
        if(reroll.getSecond() != null){

            Pair<EmbedBuilder, EmbedBuilder> pair;

            if(reroll.getSecond()){
                pair = teamEmbedBuilder(s.getTeams(reroll.getFirst()));
            }
            else {
                pair = vEmbedBuilder(s.getTeams(reroll.getFirst()));
            }

            EmbedBuilder eb1 = pair.getFirst();
            EmbedBuilder eb2 = pair.getSecond();

            event.getChannel().sendMessage(eb1.build()).queue();
            event.getChannel().sendMessage(eb2.build()).queue();

            s.increaseXp(event.getAuthor().getId());
        }
        else{
            event.getChannel().sendMessage("Faz equipas crl").queue();
        }
    }

    private static void processSimao(MessageReceivedEvent event){
        MessageChannel channel = event.getChannel();

        String msgContent = event.getMessage().getContentRaw();

        if(msgContent.equals("!play eu sou peixeiro") && event.getAuthor().getId().equals(IDSIMAO)){
            channel.sendMessage("cala a boca preto filha da puta").queue();
        }
    }

    private static void processFeed(MessageReceivedEvent event, SystemInterface s){
        String feed = s.getFeed();

        MessageChannel channel = event.getChannel();
        channel.sendMessage(feed).queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processLane(MessageReceivedEvent event, SystemInterface s){
        String lane = s.getLane();
        event.getChannel().sendMessage(lane).queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processAddFrase(MessageReceivedEvent event, SystemInterface s) {
        String[] frase = event.getMessage().getContentRaw().split(" ", 2);
        try{
            if(event.getAuthor().getId().equals(IDTIMZUDO)){
                s.addFrase(frase[1]);
                event.getChannel().sendMessage("Frase adicionada").queue();
            }
            else{
                event.getChannel().sendMessage("Tens que ter mambo grande para fazer isso").queue();
            }
        }
        catch (IOException e){
            System.out.println("erro fdd");
        }
    }

    private static void processAddFeed(MessageReceivedEvent event, SystemInterface s){
        String[] feed = event.getMessage().getContentRaw().split(" ", 2);
        try{
            if(event.getAuthor().getId().equals(IDTIMZUDO)){
                s.addFeed(feed[1]);
                event.getChannel().sendMessage("Feed adicionado").queue();
            }
            else{
                event.getChannel().sendMessage("Tens que ter mambo grande para fazer isso").queue();
            }
        }
        catch (IOException e){
            System.out.println("erro fdd");
        }
    }

    private static void processAddMain(MessageReceivedEvent event, SystemInterface s){
        String[] main = event.getMessage().getContentRaw().split(" ", 2);
        try{
            if(event.getAuthor().getId().equals(IDTIMZUDO)){
                s.addMain(main[1]);
                event.getChannel().sendMessage("Main adicionado").queue();
            }
            else{
                event.getChannel().sendMessage("Tens que ter mambo grande para fazer isso").queue();
            }
        }
        catch (IOException e){
            System.out.println("erro fdd");
        }
    }

    private static void processLista(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        Iterator<User> it = s.userIterator();
        s.increaseXp(event.getAuthor().getId());

        StringBuilder list = new StringBuilder();


        while(it.hasNext()){
            list.append(it.next()).append(" XP\n");
        }

        channel.sendMessage(list).queue();
    }

    private static void processTop(MessageReceivedEvent event, SystemInterface s){

        //Sque da p melhorar
        MessageChannel channel = event.getChannel();
        try{

            Iterator<User> it = s.topIterator();

            String[] topUsers = {"", "", ""};

            for(int i = 0; i<3 && it.hasNext(); i++){
                User user = it.next();

                topUsers[i] = user.getName() + " - " + user.getXp() + " xp";
            }

            String text = topUsers[0] + "\n" + topUsers[1] + "\n" + topUsers[2];

            EmbedBuilder eb = new EmbedBuilder();
            eb.setColor(Color.orange);
            eb.setTitle("Top 3 XP:");
            eb.setDescription(text);

            channel.sendMessage(eb.build()).queue();
        }
        catch (InvalidNumberException e){
            channel.sendMessage("Erro mamboso").queue();
        }

        s.increaseXp(event.getAuthor().getId());
    }

    private static void processUserInfo(MessageReceivedEvent event, SystemInterface s){
        MessageChannel channel = event.getChannel();
        String msgContent = event.getMessage().getContentRaw();
        String[] name = msgContent.trim().split(" ", 2);
        if(name.length != 1){
            String userId = name[1];

            try{
                User user = s.getUserName(userId);

                s.increaseXp(event.getAuthor().getId());

                EmbedBuilder eb = new EmbedBuilder();
                eb.setColor(Color.orange);
                eb.setTitle(user.getName());
                eb.setDescription(user.getXp() + " XP\n" + "\n" + user.getId() + "\n");

                channel.sendMessage(eb.build()).queue();
            }
            catch (UserDoesNotExistException e){
                channel.sendMessage("User " + userId + " não existe.").queue();
            }
        }
        else{
            channel.sendMessage("Mete um nome crl.").queue();
        }
    }

    private static void processSay(MessageReceivedEvent event){

        if(sound){
            String[] message = event.getMessage().getContentRaw().split(" ", 2);
            if(message.length != 1 && message[1].length() <= 50){
                MessageBuilder msg = new MessageBuilder();
                msg.append(message[1]);
                msg.setTTS(true);
                msg.sendTo(event.getChannel()).queue();
            }
            else{
                event.getChannel().sendMessage("Tamanho de mensagem bem cansado.").queue();
            }
        }
        else{
            event.getChannel().sendMessage("Som inativo, contacte o Timzudo").queue();
        }

    }

    private static void processInfo(MessageReceivedEvent event){
        MessageChannel channel = event.getChannel();

        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.orange);
        eb.setTitle(VERSION);

        eb.addField("Comandos:", PRINTINFO, false);
        eb.addField("Novidades:", NOVIDADES, false);

        channel.sendMessage(eb.build()).queue();
    }

    private static void processVibe(MessageReceivedEvent event, SystemInterface s){

        if(sound){
            if(Objects.requireNonNull(event.getMember()).getVoiceState().inVoiceChannel()){

                String url = s.getVibe();

                s.increaseXp(event.getAuthor().getId());
                play(event, url);
            }
            else{
                event.getTextChannel().sendMessage("Entra numa sala wi").queue();
            }
        }
        else{
            event.getChannel().sendMessage("Som inativo, contacte o Timzudo").queue();
        }

    }

    private static void processMc(MessageReceivedEvent event, SystemInterface s){

        if(sound){
            if(Objects.requireNonNull(event.getMember()).getVoiceState().inVoiceChannel()){

                String url = s.getMc();

                s.increaseXp(event.getAuthor().getId());
                play(event, url);
            }
            else{
                event.getTextChannel().sendMessage("Entra numa sala wi").queue();
            }
        }
        else{
            event.getChannel().sendMessage("Som inativo, contacte o Timzudo").queue();
        }
    }

    private static void processMambo(MessageReceivedEvent event, SystemInterface s){

        if(sound){
            if(Objects.requireNonNull(event.getMember()).getVoiceState().inVoiceChannel()){

                String url = "https://www.youtube.com/watch?v=HJg4e6XDsds&list=PLubRNjVixLnz_3Rl5ZyuFDUvLQ_90p8Kh&index=2&t";

                s.increaseXp(event.getAuthor().getId());
                play(event, url);
            }
            else{
                event.getTextChannel().sendMessage("Entra numa sala wi").queue();
            }
        }
        else{
            event.getChannel().sendMessage("Som inativo, contacte o Timzudo").queue();
        }

    }

    private static void processStop(MessageReceivedEvent event){

        Guild guild = event.getGuild();

        AudioManager audioManager = guild.getAudioManager();
        audioManager.closeAudioConnection();

        PlayerManager manager = PlayerManager.getInstance();
        manager.stop(event.getTextChannel());
    }

    private static void processWater(MessageReceivedEvent event){
        if(!event.getMember().getId().equals(IDWATER)){
            water = !water;
            if(water){
                event.getTextChannel().sendMessage("proteção de water™ ligada.").queue();
            }
            else{
                event.getTextChannel().sendMessage("proteção de water™ desligada.").queue();
            }
        }else{
            event.getChannel().sendMessage("lida water").queue();
        }

    }

    private static void processShuffle(MessageReceivedEvent event){

        PlayerManager manager = PlayerManager.getInstance();
        manager.shuffle(event.getTextChannel());
    }

    private static void processSkip(MessageReceivedEvent event){

        PlayerManager manager = PlayerManager.getInstance();
        manager.next(event.getTextChannel());
    }

    private static void processSound(MessageReceivedEvent event){
        if(event.getAuthor().getId().equals(IDTIMZUDO)){
            sound = !sound;
            if(sound){
                event.getChannel().sendMessage("Som ativo").queue();
                jda.getPresence().setStatus(OnlineStatus.ONLINE);
            }
            else{
                event.getChannel().sendMessage("Som inativo").queue();
                jda.getPresence().setStatus(OnlineStatus.DO_NOT_DISTURB);
            }
        }
        else {
            if(sound){
                event.getChannel().sendMessage("Estado: Som ativo").queue();
            }
            else{
                event.getChannel().sendMessage("Estado: Som inativo").queue();
            }
        }
    }

    private static void processSuggest(MessageReceivedEvent event, SystemInterface s) throws IOException {

        String[] message = event.getMessage().getContentRaw().split(" ", 2);
        String nickName = event.getAuthor().getName();

        s.suggest(message[1], nickName);
        event.getChannel().sendMessage("bacano").queue();
        s.increaseXp(event.getAuthor().getId());
    }

    private static void processSuggestList(MessageReceivedEvent event, SystemInterface s) throws FileNotFoundException {

        if(event.getAuthor().getId().equals(IDTIMZUDO)){
            Iterator<String> it = s.getSuggestList();

            event.getChannel().sendMessage("Sugestões:" + "\n").queue();

            StringBuilder list = new StringBuilder();

            while(it.hasNext()){
                list.append(it.next()).append(" - ").append(it.next()).append("\n");
            }
            event.getChannel().sendMessage(list).queue();
        }
    }

    private static void processV1(MessageReceivedEvent event, SystemInterface s){

        if(v1.getFirst().equals("*") && v1.getSecond().equals("*")){
            v1 = new PairClass<>(event.getMember().getId(), "*");
        }
        else if(!v1.getFirst().equals("*") && v1.getSecond().equals("*")){
            v1 = new PairClass<>(v1.getFirst(), event.getMember().getId());
            int random = new Random().nextInt(2);
            switch (random){
                case 0:
                    event.getChannel().sendMessage(s.getUser(v1.getFirst()).getName() + " tem mambo superior").queue();
                    break;
                case 1:
                    event.getChannel().sendMessage(s.getUser(v1.getSecond()).getName() + " tem mambo superior").queue();
                    break;
            }
            v1 = new PairClass<>("*", "*");
        }

    }

    private static void processPoll(MessageReceivedEvent event, SystemInterface s) {
        String[] message = event.getMessage().getContentRaw().split(" ", 2);
        String name = message[1];


        event.getChannel().sendMessage(name).queue();
        waiting = true;
        /*String id = event.getTextChannel().getLatestMessageId();

        s.addPoll(id, name);*/
    }

    private static void processIp(MessageReceivedEvent event, SystemInterface s){
        event.getChannel().sendMessage("85.242.233.34").queue();
    }


    private static void processTeste(MessageReceivedEvent event){
        /*Translate translate = TranslateOptions.getDefaultInstance().getService();
        String[] text = event.getMessage().getContentRaw().split(" ",2);

        Detection detection = translate.detect(text[1]);
        String detectedLanguage = detection.getLanguage();



        Translation translation = translate.translate(
                text[1],
                Translate.TranslateOption.sourceLanguage(detectedLanguage),
                Translate.TranslateOption.targetLanguage("en"));
        event.getChannel().sendMessage(translation.getTranslatedText()).queue();*/
    }

    private static void processLoad(MessageReceivedEvent event, SystemInterface s) throws IOException, InvalidFileException{
        File folder = new File("xp");
        folder.mkdir();

        File file = new File(folder, (event.getGuild().getName() + ".txt"));

        Scanner in = new Scanner(file);


        while(in.hasNext()){
            String id = in.nextLine().trim();
            String name = in.nextLine().trim();
            int xp = Integer.parseInt(in.nextLine().trim());
            s.loadUser(id, name, xp);
        }

        in.close();
    }

    private static void processSave(MessageReceivedEvent event, SystemInterface s) throws IOException{
        File folder = new File("xp");


        File file = new File(folder, (event.getGuild().getName() + ".txt"));

        FileWriter out = new FileWriter(file);

        Iterator<User> it = s.userIterator();

        while(it.hasNext()){
            User user = it.next();
            out.write(user.getId() + "\n" + user.getName() + "\n" + user.getXp() + "\n");
        }
        out.close();
    }


    private static void play(MessageReceivedEvent event, String url){

        String channelId = Objects.requireNonNull(event.getMember()).getVoiceState().getChannel().getId();
        Guild guild = event.getGuild();
        VoiceChannel channel = event.getGuild().getVoiceChannelById(channelId);

        AudioManager audioManager = guild.getAudioManager();
        audioManager.openAudioConnection(channel);

        PlayerManager manager = PlayerManager.getInstance();
        manager.loadAndPlay(event.getTextChannel(), url);
        manager.getGuildMusicManager(event.getGuild()).player.setVolume(20);

    }

    private static Pair<EmbedBuilder, EmbedBuilder> teamEmbedBuilder(Pair<Iterator<String>, Iterator<String>> pair){


        Iterator<String> ItA = pair.getFirst();
        Iterator<String> ItB = pair.getSecond();

        EmbedBuilder eb1 = new EmbedBuilder();
        EmbedBuilder eb2 = new EmbedBuilder();

        eb1.setColor(Color.orange);
        eb1.setTitle("Team A:");

        while(ItA.hasNext()){
            eb1.appendDescription(ItA.next() + "\n");
        }


        eb2.setColor(Color.orange);
        eb2.setTitle("Team B:");

        while(ItB.hasNext()){
            eb2.appendDescription(ItB.next() + "\n");
        }
        return new PairClass<>(eb1, eb2);

    }

    private static Pair<EmbedBuilder, EmbedBuilder> vEmbedBuilder(Pair<Iterator<String>, Iterator<String>> pair){


        Iterator<String> ItA = pair.getFirst();
        Iterator<String> ItB = pair.getSecond();

        EmbedBuilder eb1 = new EmbedBuilder();
        EmbedBuilder eb2 = new EmbedBuilder();

        eb1.setColor(Color.orange);
        eb1.setTitle("Team A:");


        eb1.appendDescription("TOP - " + ItA.next() + "\n");
        eb1.appendDescription("JNG - " + ItA.next() + "\n");
        eb1.appendDescription("MID - " + ItA.next() + "\n");
        eb1.appendDescription("BOT - " + ItA.next() + "\n");
        eb1.appendDescription("SUP - " + ItA.next() + "\n");


        eb2.setColor(Color.orange);
        eb2.setTitle("Team B:");

        eb2.appendDescription("TOP - " + ItB.next() + "\n");
        eb2.appendDescription("JNG - " + ItB.next() + "\n");
        eb2.appendDescription("MID - " + ItB.next() + "\n");
        eb2.appendDescription("BOT - " + ItB.next() + "\n");
        eb2.appendDescription("SUP - " + ItB.next() + "\n");

        return new PairClass<>(eb1, eb2);

    }

    private static void infantario(GenericGuildVoiceEvent  event){
        Member member = event.getMember();
        //criar metodo remove water

        if(member.getId().equals(IDWATER) && water){
            event.getGuild().moveVoiceMember(member, event.getGuild().getVoiceChannelById("727233940284506193")).complete();
            event.getGuild().getDefaultChannel().sendMessage("water removido com sucesso.").queue();

            AudioManager audioManager = event.getGuild().getAudioManager();
            audioManager.openAudioConnection(event.getGuild().getVoiceChannelById("727233940284506193"));

            PlayerManager manager = PlayerManager.getInstance();
            manager.loadAndPlay(event.getGuild().getDefaultChannel(), "https://www.youtube.com/watch?v=nHvo-aoN-Zc");
            manager.getGuildMusicManager(event.getGuild()).player.setVolume(20);
        }
    }

}
